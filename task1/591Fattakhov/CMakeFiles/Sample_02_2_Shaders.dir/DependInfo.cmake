# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/fartuk/graphics/repo/seminar2/Sample_02_2_Shaders.cpp" "/home/fartuk/graphics/repo/seminar2/CMakeFiles/Sample_02_2_Shaders.dir/Sample_02_2_Shaders.cpp.o"
  "/home/fartuk/graphics/repo/common/Application.cpp" "/home/fartuk/graphics/repo/seminar2/CMakeFiles/Sample_02_2_Shaders.dir/__/common/Application.cpp.o"
  "/home/fartuk/graphics/repo/common/Camera.cpp" "/home/fartuk/graphics/repo/seminar2/CMakeFiles/Sample_02_2_Shaders.dir/__/common/Camera.cpp.o"
  "/home/fartuk/graphics/repo/common/DebugOutput.cpp" "/home/fartuk/graphics/repo/seminar2/CMakeFiles/Sample_02_2_Shaders.dir/__/common/DebugOutput.cpp.o"
  "/home/fartuk/graphics/repo/common/Mesh.cpp" "/home/fartuk/graphics/repo/seminar2/CMakeFiles/Sample_02_2_Shaders.dir/__/common/Mesh.cpp.o"
  "/home/fartuk/graphics/repo/common/ShaderProgram.cpp" "/home/fartuk/graphics/repo/seminar2/CMakeFiles/Sample_02_2_Shaders.dir/__/common/ShaderProgram.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_STATIC"
  "GLFW_DLL"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "common"
  "external/Assimp/include"
  "external/glew-1.13.0/include"
  "external/GLFW/include"
  "external/GLM"
  "external/imgui"
  "external/SOIL/src/SOIL2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/fartuk/graphics/repo/external/Assimp/code/CMakeFiles/assimp.dir/DependInfo.cmake"
  "/home/fartuk/graphics/repo/external/glew-1.13.0/build/cmake/CMakeFiles/glew_s.dir/DependInfo.cmake"
  "/home/fartuk/graphics/repo/external/imgui/CMakeFiles/imgui.dir/DependInfo.cmake"
  "/home/fartuk/graphics/repo/external/SOIL/CMakeFiles/soil.dir/DependInfo.cmake"
  "/home/fartuk/graphics/repo/external/Assimp/contrib/irrXML/CMakeFiles/IrrXML.dir/DependInfo.cmake"
  "/home/fartuk/graphics/repo/external/GLFW/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
